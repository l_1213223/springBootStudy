package com.apgblogs.springbootstudy.util;

import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-17 16:24
 */
@Component
public class QuartzJobUtil {

    private static final Logger logger = LoggerFactory.getLogger(QuartzJobUtil.class);

    private static QuartzJobUtil quartzJobUtil;

    @Autowired
    private Scheduler scheduler;
    
    public QuartzJobUtil() {
        logger.info("init jobUtil");
        quartzJobUtil = this;
    }

    public static QuartzJobUtil getInstance() {
        logger.info("return  JobCreateUtil");
        return QuartzJobUtil.quartzJobUtil;
    }

    /**
     * @description 创建job
     * @author xiaomianyang
     * @date 2019-07-17 17:31
     * @param [clazz, jobName, jobGroupName, cronExpression]
     * @return void
     */
    public void addJob(Class clazz, String jobName, String jobGroupName, String cronExpression) {
        addJob(clazz, jobName, jobGroupName, cronExpression, null);
    }


    /**
     * @description 创建job
     * @author xiaomianyang
     * @date 2019-07-17 17:31
     * @param [clazz, jobName, jobGroupName, cronExpression, argMap]
     * @return void
     */
    public void addJob(Class clazz, String jobName, String jobGroupName, String cronExpression, Map<String, Object> argMap) {
        try {
            // 启动调度器
            scheduler.start();
            //构建job信息
            JobDetail jobDetail = JobBuilder.newJob(((Job) clazz.newInstance()).getClass()).withIdentity(jobName, jobGroupName).build();
            //表达式调度构建器(即任务执行的时间)
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
            //按新的cronExpression表达式构建一个新的trigger
            CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(jobName, jobGroupName).withSchedule(scheduleBuilder).build();
            //获得JobDataMap，写入数据
            if (argMap != null) {
                trigger.getJobDataMap().putAll(argMap);
            }
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 暂停job
     * @author xiaomianyang
     * @date 2019-07-17 17:31
     * @param [jobName, jobGroupName]
     * @return void
     */
    public void pauseJob(String jobName, String jobGroupName) {
        try {
            scheduler.pauseJob(JobKey.jobKey(jobName, jobGroupName));
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 恢复job
     * @author xiaomianyang
     * @date 2019-07-17 17:31
     * @param [jobName, jobGroupName]
     * @return void
     */
    public void resumeJob(String jobName, String jobGroupName) {
        try {
            scheduler.resumeJob(JobKey.jobKey(jobName, jobGroupName));
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }


    /**
     * @description 更新job频率
     * @author xiaomianyang
     * @date 2019-07-17 17:31
     * @param [jobName, jobGroupName, cronExpression]
     * @return void
     */
    public void updateJob(String jobName, String jobGroupName, String cronExpression) {
        updateJob(jobName, jobGroupName, cronExpression, null);


    }


    /**
     * @description 更新job频率和参数
     * @author xiaomianyang
     * @date 2019-07-17 17:31
     * @param [jobName, jobGroupName, cronExpression, argMap]
     * @return void
     */
    public void updateJob(String jobName, String jobGroupName, String cronExpression, Map<String, Object> argMap) {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(jobName, jobGroupName);
            // 表达式调度构建器
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
            CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
            // 按新的cronExpression表达式重新构建trigger
            trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
            //修改map
            if (argMap != null) {
                trigger.getJobDataMap().putAll(argMap);
            }
            // 按新的trigger重新设置job执行
            scheduler.rescheduleJob(triggerKey, trigger);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 更新job参数
     * @author xiaomianyang
     * @date 2019-07-17 17:30
     * @param [jobName, jobGroupName, argMap]
     * @return void
     */
    public void updateJob(String jobName, String jobGroupName, Map<String, Object> argMap) {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(jobName, jobGroupName);
            CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
            //修改map
            trigger.getJobDataMap().putAll(argMap);
            // 按新的trigger重新设置job执行
            scheduler.rescheduleJob(triggerKey, trigger);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * @description 删除job
     * @author xiaomianyang
     * @date 2019-07-17 17:30
     * @param [jobName, jobGroupName]
     * @return void
     */
    public void deleteJob(String jobName, String jobGroupName) {
        try {
            scheduler.pauseTrigger(TriggerKey.triggerKey(jobName, jobGroupName));
            scheduler.unscheduleJob(TriggerKey.triggerKey(jobName, jobGroupName));
            scheduler.deleteJob(JobKey.jobKey(jobName, jobGroupName));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @description 启动所有定时任务
     * @author xiaomianyang
     * @date 2019-07-17 17:30
     * @param []
     * @return void
     */
    public void startAllJobs() {
        try {
            scheduler.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @description 关闭所有定时任务
     * @author xiaomianyang
     * @date 2019-07-17 17:30
     * @param []
     * @return void
     */
    public void shutdownAllJobs() {
        try {
            if (!scheduler.isShutdown()) {
                scheduler.shutdown();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @description 获取所有任务列表
     * @author xiaomianyang
     * @date 2019-07-17 17:30
     * @param []
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    public List<Map<String, Object>> getAllJob() {
        GroupMatcher<JobKey> matcher = GroupMatcher.anyJobGroup();
        List<Map<String, Object>> jobList = new ArrayList<>();
        Set<JobKey> jobKeys = null;
        try {
            jobKeys = scheduler.getJobKeys(matcher);
            for (JobKey jobKey : jobKeys) {
                List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
                for (Trigger trigger : triggers) {
                    Map<String, Object> job = new HashMap<>();
                    job.put("jobName", jobKey.getName());
                    job.put("jobGroupName", jobKey.getGroup());
                    job.put("trigger", trigger.getKey());
                    Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
                    job.put("jobStatus", triggerState.name());
                    if (trigger instanceof CronTrigger) {
                        CronTrigger cronTrigger = (CronTrigger) trigger;
                        String cronExpression = cronTrigger.getCronExpression();
                        job.put("cronExpression", cronExpression);
                    }
                    jobList.add(job);
                }
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        return jobList;
    }
}
