package com.apgblogs.springbootstudy.model;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-05-14 23:08
 */
public enum ErrorStatus {

    /**
     * 后台处理错误
     */
    INTERNAL_SERVER_ERROR(500,"后台处理错误"),
    /**
     * 警告错误
     */
    INTERNAL_SERVER_WARNING(600,"警告"),
    /**
     * 资源已存在
     */
    NOT_FOUND(50001,"资源不存在"),

    /**
     * 资源已存在
     */
    ALREADY_EXIST(50002,"资源已存在"),
    ;

    private int code;

    private String msg;

    public int getCode()
    {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    ErrorStatus(int code, String msg)
    {
        this.code = code;
        this.msg = msg;
    }
}
