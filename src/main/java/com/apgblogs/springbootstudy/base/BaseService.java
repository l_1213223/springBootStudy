package com.apgblogs.springbootstudy.base;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-04-28 下午 12:33
 */
public class BaseService<D extends BaseDao,M extends BaseModel> {

    @Autowired
    protected D dao;

    /**
     * @description 查询全部
     * @author xiaomianyang
     * @date 2019-04-28 下午 01:03
     * @param [m, page]
     * @return java.util.List<M>
     */
    public PageInfo selectAll(M m, Page page){
        page=PageHelper.startPage(page.getPageNum(),page.getPageSize());
        dao.selectAll(m,page);
        PageInfo pageInfo=new PageInfo<>(page.getResult());
        return pageInfo;
    }
}
