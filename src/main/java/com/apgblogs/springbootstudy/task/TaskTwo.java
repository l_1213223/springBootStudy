package com.apgblogs.springbootstudy.task;

import com.apgblogs.springbootstudy.service.UserServiceImpl;
import com.google.gson.Gson;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-18 10:21
 */
public class TaskTwo implements Job {

    private final Logger logger= LoggerFactory.getLogger(TaskOne.class);

    @Autowired
    private UserServiceImpl userService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext){
        logger.info("用户列表2：{}",new Gson().toJson(userService.getUserList()));
    }
}
