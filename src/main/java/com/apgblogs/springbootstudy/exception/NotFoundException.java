package com.apgblogs.springbootstudy.exception;

import com.apgblogs.springbootstudy.model.ErrorStatus;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-05-14 23:24
 */
public class NotFoundException extends GlobalException {

    public NotFoundException(ErrorStatus errorStatus) {
        super(errorStatus);
    }

    public NotFoundException(String message, ErrorStatus errorStatus) {
        super(message, errorStatus);
    }
}
