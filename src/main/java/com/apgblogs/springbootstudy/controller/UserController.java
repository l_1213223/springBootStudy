package com.apgblogs.springbootstudy.controller;

import com.apgblogs.springbootstudy.entity.TUserEntity;
import com.apgblogs.springbootstudy.exception.GlobalException;
import com.apgblogs.springbootstudy.exception.NotFoundException;
import com.apgblogs.springbootstudy.model.ErrorStatus;
import com.apgblogs.springbootstudy.service.UserServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-06-13 15:54
 */
@Api(description = "用户增删改查接口")
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    /**
     * @description 查询所有用户
     * @author xiaomianyang
     * @date 2019-06-13 16:05
     * @param []
     * @return java.util.List<TUserEntity>
     */
    @ApiOperation(value="获取所有用户信息",notes = "查询所有用户信息")
    @GetMapping
    public List<TUserEntity> getAllUser(){
        return userService.getUserList();
    }

    /**
     * @description 获取单个用户
     * @author xiaomianyang
     * @date 2019-06-13 16:05
     * @param [id]
     * @return TUserEntity
     */
    @ApiOperation(value="获取单个用户信息",notes="通过用户id查询用户信息")
    @ApiImplicitParam(name = "id",value="用户id",required = true,paramType = "path",dataType = "String")
    @GetMapping("{id}")
    public TUserEntity getUser(@PathVariable("id")String id) throws GlobalException{
        return userService.getUser(id);
    }

    /**
     * @description 创建用户
     * @author xiaomianyang
     * @date 2019-06-13 16:05
     * @param [tUserEntity]
     * @return TUserEntity
     */
    @ApiOperation(value="新增用户",notes = "新增用户")
    @PostMapping
    public TUserEntity insertUser(@RequestBody TUserEntity tUserEntity){
        return userService.insertUser(tUserEntity);
    }

    /**
     * @description 更新用户
     * @author xiaomianyang
     * @date 2019-06-13 16:05
     * @param [tUserEntity]
     * @return TUserEntity
     */
    @ApiOperation(value="更新用户",notes = "修改用户部分信息")
    @PatchMapping
    public TUserEntity updateUser(@RequestBody TUserEntity tUserEntity){
        return userService.updateUser(tUserEntity);
    }

    /**
     * @description 删除用户
     * @author xiaomianyang
     * @date 2019-06-13 16:13
     * @param [id]
     * @return boolean
     */
    @ApiOperation(value="删除用户",notes = "通过id删除用户")
    @ApiImplicitParam(name = "id",value="用户id",required = true,paramType = "path",dataType = "String")
    @DeleteMapping("{id}")
    public boolean deleteUser(@PathVariable("id")String id){
        userService.deleteUser(id);
        return true;
    }
}
