package com.apgblogs.springbootstudy.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-05-15 13:09
 */
@RestController
@RequestMapping("/secure")
public class SecureController {

    @GetMapping
    public String sayHello(){
        return "secure hello";
    }
}
