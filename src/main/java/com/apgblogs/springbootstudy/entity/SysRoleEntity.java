package com.apgblogs.springbootstudy.entity;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-06-26 17:50
 */
@Entity
@Table(name = "sys_role", schema = "test", catalog = "")
public class SysRoleEntity {
    private String id;
    private String name;

    @Id
    @Column(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SysRoleEntity that = (SysRoleEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
